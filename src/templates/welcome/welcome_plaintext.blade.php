Hello, {{$name}}!
Thank you for joining {{config('app.name')}}!
Your account has been created, and you may login at {{config('app.url') . '/login'}}.